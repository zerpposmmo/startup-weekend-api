let express = require('express');
let bodyParser = require('body-parser');
let router = express.Router();
let modeHandler = require('../handlers/modeHandler');
let mqtt = require('mqtt');


let jsonParser = bodyParser.json();
let modehandler = new modeHandler();

let client = mqtt.connect('mqtt://localhost');
let info = {};
let comfortTemp = 25.1;
let mode = 'comfort';

client.on('connect', () => {
    client.subscribe('sensor/4C:65:A8:D3:F7:F0', (err) => {
        if (!err) {
            client.publish('presence', 'Hello mqtt');
        }
    })
});

client.on('message', (topic, message) => {
    return handleMessage(message);
});

router.post('/change-mode', jsonParser,function (req, res) {
    if(req.body.mode) {
        if(req.body.mode !== mode) {
            mode = modehandler.switchMode(req.body.mode);
            if(mode !== 'unknown') {
                res.status(200).json({
                    key: 'mode.changed',
                    mode: mode
                });
            } else {
                res.status(400).json({
                    key: 'unknown.mode',
                    mode: req.body.mode
                });
            }
        } else {
            res.status(400).json({
                key: 'already.current.mode',
                mode: req.body.mode
            });
        }
    }
});

router.get('/modes', function (req, res) {
   res.status(200).json({
       modes: modehandler.modes,
       currentMode: modehandler.mode,
   }) ;
});

router.get('/sensor-info', function (req, res) {
    res.status(200).send(info);
});

router.get('/comfort-temp', function (req, res) {
   res.status(200).send({
       temp: comfortTemp
   })
});

router.post('/comfort-temp', jsonParser,function (req, res) {
    if(req.body.temp) {
        comfortTemp = req.body.temp;
    }
    res.status(200).json({
        key: 'temp.changed',
        temp: comfortTemp
    });
});

function handleMessage(message) {
    info = message.toString('utf8');
    console.log('Current temperature : ' + parseFloat(JSON.parse(info).temp));
    if(parseFloat(JSON.parse(info).temp) > comfortTemp + 1 && mode === 'comfort') {
        mode = modehandler.switchMode('stop');
        console.log('Mode changed due to high temperature : ' + mode);
    } else if(parseFloat(JSON.parse(info).temp) < comfortTemp - 1  && mode === 'stop') {
        mode = modehandler.switchMode('comfort');
        console.log('Mode changed due to low temperature : ' + mode);
    }
}

module.exports = router;
