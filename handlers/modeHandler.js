const Gpio = require('onoff').Gpio;

class modeHandler {

    constructor() {
        this.pin0 = new Gpio(5, 'out');
        this.pin1 = new Gpio(6, 'out');
        this.modes = ["eco", "comfort", "holidays", "stop"];
        this.mode = 'comfort';
        this.pin0.writeSync(0);
        this.pin1.writeSync(0);
    }

    switchMode(mode) {
        switch (mode) {
            case 'comfort':
                this.pin0.writeSync(0);
                this.pin1.writeSync(0);
                break;
            case 'holidays':
                this.pin0.writeSync(0);
                this.pin1.writeSync(1);
                break;
            case 'stop':
                this.pin0.writeSync(1);
                this.pin1.writeSync(0);
                break;
            case 'eco':
                this.pin0.writeSync(1);
                this.pin1.writeSync(1);
                break;
            default:
                return 'unknown'
        }
        this.mode = mode;
        return mode;
    }
}

module.exports = modeHandler;