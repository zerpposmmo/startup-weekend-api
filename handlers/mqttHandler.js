let mqtt = require('mqtt');

class mqttHandler {
    constructor() {

        this.info = {};

        this.client = mqtt.connect('mqtt://localhost');

        this.client.on('connect', () => {
            this.client.subscribe('sensor/4C:65:A8:D3:F7:F0', (err) => {
                if (!err) {
                    this.client.publish('presence', 'Hello mqtt');
                }
            })
        });

        this.client.on('message', (topic, message) => {
            this.info = message.toString('utf8');
        });
    }

    getCaptorInfo() {
        return this.info;
    }
}

module.exports = mqttHandler;
